//
//  LanguageModel.swift
//  Verses
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit


class LanguageModel: NSObject {
    private var name: String!
    private var code: String!
    private var preferredFontCode: FontCode!

    static let Empty = try! LanguageModel(code: " ", name: " ", fontCode: .LatinFont)

    private override init() {
        super.init()
    }

    init(code _code: String, name _name: String, fontCode _preferredFontCode: FontCode) throws {
        super.init()

        if _code.isEmpty || _name.isEmpty {
            throw Exception.InvalidArgument
        }

        self.name = _name
        self.code = _code
        self.preferredFontCode = _preferredFontCode
    }

    func getCode() -> String {
        return code
    }

    func getName() -> String {
        return name
    }

    func getPreferredFontCode() -> FontCode {
        return preferredFontCode
    }
}
