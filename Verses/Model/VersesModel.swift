//
//  VersesModel.swift
//  Verses
//
//  Created by Darko VUKOTIC on 21.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class VersesModel: NSObject {
    private var versesStr: String!

    static let Empty = try! VersesModel(" ")

    private override init() {
        super.init()
    }

    init(_ _versesStr: String) throws {
        super.init()

        if _versesStr.isEmpty {
            throw Exception.InvalidArgument
        }

        self.versesStr = _versesStr
    }

    func getVersesStr() -> String {
        return self.versesStr
    }
}
