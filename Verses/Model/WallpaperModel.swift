//
//  WallpaperModel.swift
//  Verses
//
//  Created by Darko VUKOTIC on 23.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class WallpaperModel: NSObject {
    private var fileName: String!

    private override init() {
        super.init()
    }

    init(_ _fileName: String) throws {
        super.init()

        if _fileName.isEmpty {
            throw Exception.InvalidArgument
        }

        fileName = _fileName
    }

    func getFileName() -> String { return fileName }
}
