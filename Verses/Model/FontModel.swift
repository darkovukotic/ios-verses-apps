//
//  FontModel.swift
//  Verses
//
//  Created by Darko VUKOTIC on 28.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

enum FontCode {
    case LatinFont
    case HindiFont
    case CyrillicFont
    case ArabicFont
}

class FontModel: NSObject {
    private var name: String!
    private var code: FontCode!
    private var preferredSize: Int!

    static let Empty = try! FontModel(code: .LatinFont, name: " ", preferredSize: 0)

    private override init() {
        super.init()
    }

    init(code _code: FontCode, name _name: String, preferredSize _preferredSize: Int) throws {
        super.init()

        if _preferredSize < 0 || _name.isEmpty {
            throw Exception.InvalidArgument
        }

        self.name = _name
        self.code = _code
        self.preferredSize = _preferredSize
    }

    func getCode() -> FontCode {
        return code
    }

    func getName() -> String {
        return name
    }

    func getPreferredSize() -> Int {
        return preferredSize
    }

}
