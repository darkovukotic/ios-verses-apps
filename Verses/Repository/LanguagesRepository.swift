//
//  LanguagesRepository.swift
//  Verses
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

typealias LanguagesCallback = (CallbackResult, [LanguageModel]) -> Void

protocol LanguagesRepository {
    func getLanguages(_ : @escaping LanguagesCallback) throws
}
