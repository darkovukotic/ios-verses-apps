//
//  WallpapersRepository.swift
//  Verses
//
//  Created by Darko VUKOTIC on 23.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

typealias WallpapersCallback = (CallbackResult, [WallpaperModel]) -> Void

protocol WallpapersRepository {
    func getWallpapers(_ : @escaping WallpapersCallback) throws
}
