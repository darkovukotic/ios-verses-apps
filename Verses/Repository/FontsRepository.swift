//
//  FontsRepository.swift
//  Verses
//
//  Created by Darko VUKOTIC on 28.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

typealias FontsCallback = (CallbackResult, [FontModel]) -> Void

protocol FontsRepository {
    func getFonts(_ : @escaping FontsCallback) throws
}

