//
//  FontsRepositoryObject.swift
//  Verses
//
//  Created by Darko VUKOTIC on 28.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class FontsRepositoryObject: NSObject, FontsRepository {
    func getFonts(_ callback: @escaping FontsCallback) throws {
        do {
            let fonts = try [
                FontModel(code: .ArabicFont, name: "_PDMS_Saleem_QuranFont", preferredSize: 25),
                FontModel(code: .CyrillicFont, name: "Miroslav-Normal", preferredSize: 24),
                FontModel(code: .HindiFont, name: "HIMALAYA-TT-FONT", preferredSize: 22),
                FontModel(code: .LatinFont, name: "EagleLake-Regular", preferredSize: 20)
            ]

            callback(.Success, fonts)

        } catch {
            callback(.Fail, [FontModel]())
        }
    }


}
