//
//  VersesRepository.swift
//  Verses
//
//  Created by Darko VUKOTIC on 23.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

protocol VersesRepository {
    func getVerses(type: VersesType, _ : @escaping VersesCallback) throws
}
