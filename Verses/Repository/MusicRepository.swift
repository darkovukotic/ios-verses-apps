//
//  MusicRepository.swift
//  Verses
//
//  Created by Darko VUKOTIC on 2.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

typealias MusicFileNamesCallback = (CallbackResult, [String]) -> Void

protocol MusicRepository {
    func getMusicFileNames(_ : @escaping MusicFileNamesCallback) throws
}
