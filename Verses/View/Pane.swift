//
//  Pane.swift
//  Verses
//
//  Created by Darko VUKOTIC on 23.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

protocol Pane: class {
    // life cycle
    func load()
    func unload()

    // ui events
    //...
}
