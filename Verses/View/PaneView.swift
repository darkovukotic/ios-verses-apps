//
//  PaneView.swift
//  Verses
//
//  Created by Darko VUKOTIC on 15.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//



protocol PaneView: class {
    // messages
    func showMessage(_ message: Message)

    // mvp
    func setPresenter(_ presenter: PanePresenter)
}


