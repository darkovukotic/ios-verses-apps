//
//  PaneViewAbstract.swift
//  Verses
//
//  Created by Darko VUKOTIC on 15.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class PaneViewAbstract: PaneView, Pane {

    func load() {
        // TODO
    }

    func unload() {
        // TODO
    }

    func showMessage(_ message: Message) {
        // TODO
    }

    func setPresenter(_ presenter: PanePresenter) {}


}
