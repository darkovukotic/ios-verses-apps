//
//  Exception.swift
//  Verses
//
//  Created by Darko VUKOTIC on 21.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import Foundation

enum Exception: Error {
    case ForbiddenCode
    case NoObjectOrEmpty
    case NotImplemented
    case InvalidArgument

    case CantStartSpeaking
}
