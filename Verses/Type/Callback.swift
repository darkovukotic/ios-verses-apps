//
//  Callback.swift
//  Verses
//
//  Created by Darko VUKOTIC on 1.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

typealias Callback = () -> Void

