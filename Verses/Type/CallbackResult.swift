//
//  CallbackResult.swift
//  Verses
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import Foundation

enum CallbackResult: Int {
    case Fail
    case Success
}

