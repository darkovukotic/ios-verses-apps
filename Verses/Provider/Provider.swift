//
//  Provider.swift
//  Verses
//
//  Created by Darko VUKOTIC on 28.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class Provider: NSObject {
    static let instance = Provider()

    private override init() {
        super.init()
    }

    // adapters
    private var preferencesAdapter: PreferencesAdapter?
    private var localeAdapter: LocaleAdapter?
    private var musicAdapter: MusicAdapter?
    private var speakingAdapter: SpeakingAdapter?

    func providePreferencesAdapter() throws -> PreferencesAdapter {
        if preferencesAdapter == nil {
            preferencesAdapter = PreferencesAdapterObject()
        }

        return preferencesAdapter!
    }

    func provideLocaleAdapter() throws -> LocaleAdapter {
        if localeAdapter == nil {
            localeAdapter = LocaleAdapterObject()
        }

        return localeAdapter!
    }

    func provideMusicAdapter() throws -> MusicAdapter {
        if musicAdapter == nil {
            musicAdapter = MusicAdapterObject()
        }

        return musicAdapter!
    }

    func provideSpeakingAdapter() throws -> SpeakingAdapter {
        if speakingAdapter == nil {
            speakingAdapter = SpeakingAdapterObject()
        }

        return speakingAdapter!
    }

    // logic
    private var versesLogic: VersesLogic?
    private var wallpapersLogic: WallpapersLogic?
    private var languagesLogic: LanguagesLogic?
    private var fontsLogic: FontsLogic?
    private var musicLogic: MusicLogic?

    func provideVersesLogic() throws -> VersesLogic {
        if versesLogic == nil {
            versesLogic = try VersesLogic.Builder()
                .repository(provideVersesRepository())
                .build()
        }

        return versesLogic!
    }

    func provideWallpapersLogic() throws -> WallpapersLogic {
        if wallpapersLogic == nil {
            wallpapersLogic = try WallpapersLogic.Builder()
                .preferences(providePreferencesAdapter())
                .repository(provideWallpapersRepository())
                .build()
        }

        return wallpapersLogic!
    }

    func provideLanguagesLogic() throws -> LanguagesLogic {
        if languagesLogic == nil {
            languagesLogic = try LanguagesLogic.Builder()
                .locale(provideLocaleAdapter())
                .preferences(providePreferencesAdapter())
                .repository(provideLanguagesRepository())
                .build()
        }

        return languagesLogic!
    }

    func provideFontsLogic() throws -> FontsLogic {
        if fontsLogic == nil {
            fontsLogic = try FontsLogic.Builder()
                .repository(provideFontsRepository())
                .build()
        }

        return fontsLogic!
    }

    func provideMusicLogic() throws -> MusicLogic {
        if musicLogic == nil {
            musicLogic = try MusicLogic.Builder()
                .repository(provideMusicRepository())
                .adapter(provideMusicAdapter())
                .build()
        }

        return musicLogic!
    }

    // repositories
    private var versesRepository: VersesRepository?
    private var wallpapersRepository: WallpapersRepository?
    private var languagesRepository: LanguagesRepository?
    private var fontsRepository: FontsRepository?
    private var musicRepository: MusicRepository?

    func provideVersesRepository() throws -> VersesRepository {
        if versesRepository == nil {
            // TODO
        }

        return versesRepository!
    }

    func provideWallpapersRepository() throws -> WallpapersRepository {
        if wallpapersRepository == nil {
            // TODO
        }

        return wallpapersRepository!
    }

    func provideLanguagesRepository() throws -> LanguagesRepository {
        if languagesRepository == nil {
            // TODO
        }

        return languagesRepository!
    }

    func provideFontsRepository() throws -> FontsRepository {
        if fontsRepository == nil {
            fontsRepository = FontsRepositoryObject()
        }

        return fontsRepository!
    }

    func provideMusicRepository() throws -> MusicRepository {
        if musicRepository == nil {
            musicRepository = MusicRepositoryObject()
        }

        return musicRepository!
    }
}
