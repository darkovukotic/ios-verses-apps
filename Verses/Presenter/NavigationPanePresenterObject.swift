//
//  NavigationPanePresenterObject.swift
//  Verses
//
//  Created by Darko VUKOTIC on 15.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class NavigationPanePresenterObject: NavigationPanePresenter, PanePresenter {
    private var music: MusicLogic!
    private var wallpapers: WallpapersLogic!
    private var preferences: PreferencesAdapter!
    private var speaking: SpeakingAdapter!
    private weak var view: PaneView?

    private init() {}

    init(view: NavigationPaneView?, music: MusicLogic?, wallpapers: WallpapersLogic?, preferences: PreferencesAdapter?, speaking: SpeakingAdapter?) throws {

        guard view != nil && music != nil && wallpapers != nil && preferences != nil && speaking != nil else {
            throw Exception.InvalidArgument
        }

        self.view = view
        self.music = music
        self.wallpapers = wallpapers
        self.preferences = preferences
        self.speaking = speaking
    }

    class Builder {
        private weak var mView: NavigationPaneView?
        private var mMusic: MusicLogic?
        private var mWallpapers: WallpapersLogic?
        private var mPreferences: PreferencesAdapter?
        private var mSpeaking: SpeakingAdapter?

        func view(_ _view: NavigationPaneView) -> Builder {
            mView = _view
            return self
        }

        func music(_ _music: MusicLogic) -> Builder {
            mMusic = _music
            return self
        }

        func wallpapers(_ _wallpapers: WallpapersLogic) -> Builder {
            mWallpapers = _wallpapers
            return self
        }

        func preferences(_ _preferences: PreferencesAdapter) -> Builder {
            mPreferences = _preferences
            return self
        }

        func speaking(_ _speaking: SpeakingAdapter) -> Builder {
            mSpeaking = _speaking
            return self
        }

        func build() throws -> NavigationPanePresenterObject {
            return try NavigationPanePresenterObject.init(view: mView, music: mMusic, wallpapers: mWallpapers, preferences: mPreferences, speaking: mSpeaking)
        }
    }

    func onClickVersesPaneBtn() {
        guard self.view != nil else {
            print("No view object")
            return
        }

        let view = self.view! as! NavigationPaneView

        view.loadPane(PaneType.Verses)
    }

    func onClickWallpapersPaneBtn() {
        guard self.view != nil else {
            print("No view object")
            return
        }

        let view = self.view! as! NavigationPaneView

        view.loadPane(PaneType.Wallpapers)
    }

    func onClickSettingsPaneBtn() {
        guard self.view != nil else {
            print("No view object")
            return
        }

        let view = self.view! as! NavigationPaneView

        view.loadPane(PaneType.Settings)
    }

    func onAppActive() {
        guard self.view != nil else {
            print("No view object")
            return
        }

        let view = self.view! as! NavigationPaneView

        // set background picture
        guard let wallpapers = self.wallpapers else {
            print("No wallpapers logic object")
            return
        }

        var wallpaper: WallpaperModel?
        do {
            wallpaper = try wallpapers.getRandomWallpaper()
            view.setBackgroundImage(wallpaper!.getFileName())
        } catch {
            print("Failed to set background image \"\(wallpaper?.getFileName() ?? "null")\" on App activation")
        }

        // start music if enabled
        guard let music = self.music else {
            print("No music logic object")
            return
        }

        guard let preferences = self.preferences else {
            print("No preferences adapter object")
            return
        }

        if preferences.getIsMusicEnabled() {
            do { try music.play() }
            catch {
                print("Failed to play music on App activation")
            }
        }
    }

    func onAppInactive() {
        // stop music
        if let music = self.music {
            music.stop()
        } else {
            print("Failed to stop music on App exit")
        }

        // stop speaking
        if let speaking = self.speaking {
            speaking.stop()
        } else {
            print("Failed to stop speaking on App exit")
        }
    }

    func viewDidShow() {
        // TODO
    }

    func viewHasGone() {
        // TODO
    }
}
