//
//  VersesPaneContract.swift
//  Verses
//
//  Created by Darko VUKOTIC on 15.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//


protocol VersesPaneView: PaneView {
    func disableReadBtn()
    func enableReadBtn()
    func setVersesText(text: String, font: FontModel)
    func getVersesText() -> String
}

protocol VersesPanePresenter: PanePresenter {
    func onClickReadVersesBtn()
    func onClickNextVersesBtn()
    func onClickShareVersesBtn()
}

