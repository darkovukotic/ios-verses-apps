//
//  VersesCallback.swift
//  Verses
//
//  Created by Darko VUKOTIC on 15.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

typealias VersesCallback = (CallbackResult, VersesType, VersesModel) -> Void

