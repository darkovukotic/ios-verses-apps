//
//  NavigationPaneContract.swift
//  Verses
//
//  Created by Darko VUKOTIC on 15.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//



protocol NavigationPaneView: PaneView {
    func loadPane(_ pane: PaneType)
    func onAppActive()
    func onAppInactive()
    func setBackgroundImage(_ imageFileName: String)
}

protocol NavigationPanePresenter: PanePresenter {
    func onClickVersesPaneBtn()
    func onClickWallpapersPaneBtn()
    func onClickSettingsPaneBtn()
    func onAppActive()
    func onAppInactive()
}

