//
//  MusicAdapterObject.swift
//  Verses
//
//  Created by Darko VUKOTIC on 2.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit
import AVFoundation

class MusicAdapterObject: NSObject, MusicAdapter, AVAudioPlayerDelegate {
    private var player : AVAudioPlayer? = nil
    private let Volume : Float = 0.1
    private static let bussySemaphore = DispatchSemaphore(value: 1)
    private weak var delegate: OnMusicEndDelegate?

    func setOnMusicEndDelegate(_ _delegate: OnMusicEndDelegate?) {
        self.delegate = _delegate
    }

    func play(_ fileName: String) throws {
        MusicAdapterObject.bussySemaphore.wait()
        defer { MusicAdapterObject.bussySemaphore.signal() }

        guard !fileName.isEmpty else {
            throw Exception.InvalidArgument
        }

        if player != nil {
            _stop()
        }

        // bundle is different for app and tests so it must be specified
        let data = NSDataAsset(name: fileName, bundle: Bundle(for: type(of: self)))
        if data == nil {
            throw Exception.NoObjectOrEmpty
        }

        try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        try AVAudioSession.sharedInstance().setActive(true)

        try player = AVAudioPlayer(data: data!.data, fileTypeHint: AVFileType.mp3.rawValue)
        if player == nil {
            throw Exception.NoObjectOrEmpty
        }

        player!.volume = Volume
        player!.numberOfLoops = 0 // play just once
        player!.delegate = self

        player!.play()
    }

    func stop() {
        MusicAdapterObject.bussySemaphore.wait()
        defer { MusicAdapterObject.bussySemaphore.signal() }

        _stop()
    }

    func isPlaying() -> Bool {
        MusicAdapterObject.bussySemaphore.wait()
        defer { MusicAdapterObject.bussySemaphore.signal() }

        return player != nil && player!.isPlaying
    }

    private func _stop() {
        if player != nil {
            player!.stop()
            player = nil
        }
    }

    // AVAudioPlayerDelegate

    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        guard let delegate = delegate else {
            return
        }

        delegate.onMusicEnd()
    }

}
