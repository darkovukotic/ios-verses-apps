//
//  SpeakingAdapter.swift
//  Verses
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

protocol SpeakingAdapter {
    func isSpeaking() -> Bool
    func isLanguageCodeSupported(_ : String) throws -> Bool
    func start(text: String, languageCode: String) throws
    func stop()
}
