//
//  PreferencesAdapterObject.swift
//  Verses
//
//  Created by Darko VUKOTIC on 1.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit



class PreferencesAdapterObject: NSObject, PreferencesAdapter {
    private let settings = UserDefaults.standard

    private enum Key: String {
        case CurrentLanguageCode
        case LastSelectedWallpaperIndex
        case IsMusicEnabled
    }

    private let CurrentLanguageCode_defaultValue = ""
    private let LastSelectedWallpaperIndex_defaultValue = String(0)
    private let IsMusicEnabled_defaultValue = String(true)

    // CurrentLanguageCode
    func getCurrentLanguageCode() -> String {
        let val = settings.string(forKey: Key.CurrentLanguageCode.rawValue) ?? CurrentLanguageCode_defaultValue
        return val
    }

    func saveCurrentLanguageCode(_ code: String) throws {
        if code.isEmpty {
            throw Exception.InvalidArgument
        }

        settings.set(code, forKey: Key.CurrentLanguageCode.rawValue)
    }

    // LastSelectedWallpaperIndex
    func getLastSelectedWallpaperIndex() -> Int {
        let val = settings.string(forKey: Key.LastSelectedWallpaperIndex.rawValue) ?? LastSelectedWallpaperIndex_defaultValue
        return Int(val)!
    }

    func saveLastSelectedWallpaperIndex(_ index: Int) throws {
        if index < 0 {
            throw Exception.InvalidArgument
        }

        settings.set(String(index), forKey: Key.LastSelectedWallpaperIndex.rawValue)
    }

    // IsMusicEnabled
    func getIsMusicEnabled() -> Bool {
        let val = settings.string(forKey: Key.IsMusicEnabled.rawValue) ?? IsMusicEnabled_defaultValue
        return Bool(val)!
    }

    func saveIsMusicEnabled(_ enabled: Bool) throws {
        settings.set(String(enabled), forKey: Key.IsMusicEnabled.rawValue)
    }

}
