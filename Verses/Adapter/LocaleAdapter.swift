//
//  LocaleAdapter.swift
//  Verses
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

protocol LocaleAdapter {
    func getPhoneLanguageCode() throws -> String
}
