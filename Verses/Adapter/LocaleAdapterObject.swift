//
//  LocaleAdapterObject.swift
//  Verses
//
//  Created by Darko VUKOTIC on 1.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class LocaleAdapterObject: NSObject, LocaleAdapter {

    func getPhoneLanguageCode() throws -> String {
        var code : String? = Locale.current.languageCode

        if code == nil || code!.count != 2 {
            code = Locale.preferredLanguages[0]

            if code == nil || code!.count < 2 {
                print("LocaleAdapterObject::getPhoneLanguageCode() failed, default to English")
                code = "en"
            }
            else if code!.count > 2 {
                let end = code!.index(code!.startIndex, offsetBy: 2)
                code = String (code![..<end])
            }
        }

        return code!
    }


}
