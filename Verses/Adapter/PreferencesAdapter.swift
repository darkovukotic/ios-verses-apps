//
//  PreferencesAdapter.swift
//  Verses
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

protocol PreferencesAdapter {
    func getCurrentLanguageCode() -> String
    func saveCurrentLanguageCode(_ : String) throws

    func getLastSelectedWallpaperIndex() -> Int
    func saveLastSelectedWallpaperIndex(_ : Int) throws

    func getIsMusicEnabled() -> Bool
    func saveIsMusicEnabled(_ : Bool) throws
}
