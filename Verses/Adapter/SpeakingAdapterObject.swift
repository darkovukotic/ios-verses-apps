//
//  SpeakingAdapterObject.swift
//  Verses
//
//  Created by Darko VUKOTIC on 1.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit
import AVFoundation

class SpeakingAdapterObject: NSObject, SpeakingAdapter {
    private var synthesizer: AVSpeechSynthesizer?
    private var utterance: AVSpeechUtterance?
    private let defaultPitch: Float = 0.8
    private let defaultRate: Float = 0.5
    private static let bussySemaphore = DispatchSemaphore(value: 1)

    func isSpeaking() -> Bool {
        SpeakingAdapterObject.bussySemaphore.wait()
        defer { SpeakingAdapterObject.bussySemaphore.signal() }

        return synthesizer != nil && synthesizer!.isSpeaking
    }

    func isLanguageCodeSupported(_ code: String) throws -> Bool {
        SpeakingAdapterObject.bussySemaphore.wait()
        defer { SpeakingAdapterObject.bussySemaphore.signal() }

        if code.isEmpty {
            throw Exception.InvalidArgument
        }

        return AVSpeechSynthesisVoice(language: code) != nil
    }

    func start(text: String, languageCode: String) throws {
        SpeakingAdapterObject.bussySemaphore.wait()
        defer { SpeakingAdapterObject.bussySemaphore.signal() }

        if text.isEmpty || languageCode.isEmpty {
            throw Exception.InvalidArgument
        }

        if synthesizer != nil {
            _stop()
        }

        synthesizer = AVSpeechSynthesizer()
        if synthesizer == nil {
            throw Exception.CantStartSpeaking
        }

        utterance = AVSpeechUtterance(string: text)
        if utterance == nil {
            throw Exception.CantStartSpeaking
        }

        utterance!.voice = AVSpeechSynthesisVoice(language: languageCode)
        if utterance!.voice == nil {
            utterance = nil
            throw Exception.CantStartSpeaking
        }

        utterance!.pitchMultiplier = defaultPitch
        utterance!.rate = defaultRate
        synthesizer!.speak(utterance!)
    }

    func stop() {
        SpeakingAdapterObject.bussySemaphore.wait()
        defer { SpeakingAdapterObject.bussySemaphore.signal() }

        _stop()
    }

    private func _stop() {
        if synthesizer != nil {
            if synthesizer!.isSpeaking {
                synthesizer!.stopSpeaking(at: /*.word*/ .immediate)
                synthesizer = nil
            }
        }

        if utterance != nil {
            utterance = nil
        }
    }

}
