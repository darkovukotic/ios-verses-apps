//
//  MusicAdapter.swift
//  Verses
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

protocol OnMusicEndDelegate: class {
    func onMusicEnd()
}

protocol MusicAdapter {
    func play(_ : String) throws
    func stop()
    func isPlaying() -> Bool
    func setOnMusicEndDelegate(_ : OnMusicEndDelegate?)
}
