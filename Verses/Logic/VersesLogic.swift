//
//  Verses.swift
//  VersesLogic
//
//  Created by Darko VUKOTIC on 21.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

enum VersesType: Int {
    case Current
    case Random
    case Next
}

class VersesLogic: NSObject {
    private var currentVerses: VersesModel?
    private var repository: VersesRepository!

    private override init() {
        super.init()
    }

    private init(repository _repository: VersesRepository?) throws {
        super.init()

        if _repository == nil {
            throw Exception.InvalidArgument
        }

        self.repository = _repository
    }

    class Builder {
        private var mRepository: VersesRepository?

        func repository(_ _repository: VersesRepository) -> Builder {
            mRepository = _repository
            return self
        }

        func build() throws -> VersesLogic {
            return try VersesLogic.init(repository: mRepository)
        }
    }

    func getVerses(type: VersesType, _ callback: @escaping VersesCallback) throws {

        switch type {
            case .Random, .Next:
                try repository.getVerses(
                    type: type,
                    { (result, type, verses) in

                        if result != CallbackResult.Success {
                            Tools.executeInMainQueue {
                                callback(.Fail, type, VersesModel.Empty)
                            }
                            return
                        }

                        self.currentVerses = verses

                        Tools.executeInMainQueue {
                            callback(result, type, verses)
                        }
                    }
                )

            case .Current:
                getCurrentVerses(callback)
        }
    }

    private func getCurrentVerses(_ callback: VersesCallback) {
        callback(
            currentVerses != nil ? .Success : .Fail,
            .Current,
            currentVerses != nil ? currentVerses! : VersesModel.Empty
        )
    }

    

}
