//
//  LanguagesLogic.swift
//  Verses
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class LanguagesLogic: NSObject {
    private var languages: [LanguageModel]?
    private var repository: LanguagesRepository!
    private var preferences: PreferencesAdapter!
    private var locale: LocaleAdapter!
    // used for everybody to wait until load ends
    private static let loadSemaphore = DispatchSemaphore(value: 1)

    private override init() {
        super.init()
    }

    private init(
        repository _repository: LanguagesRepository?,
        preferences _preferences: PreferencesAdapter?,
        locale _locale: LocaleAdapter?
        ) throws {

        super.init()

        if _repository == nil || _preferences == nil || _locale == nil {
            throw Exception.InvalidArgument
        }

        self.repository = _repository!
        self.preferences = _preferences!
        self.locale = _locale!

        loadLanguages()
    }

    class Builder {
        private var mRepository: LanguagesRepository?
        private var mPreferences: PreferencesAdapter?
        private var mLocale: LocaleAdapter?

        func repository(_ _repository: LanguagesRepository) -> Builder {
            mRepository = _repository
            return self
        }

        func preferences(_ _preferences: PreferencesAdapter) -> Builder {
            mPreferences = _preferences
            return self
        }

        func locale(_ _locale: LocaleAdapter) -> Builder {
            mLocale = _locale
            return self
        }

        func build() throws -> LanguagesLogic {
            return try LanguagesLogic.init(repository: mRepository, preferences: mPreferences, locale: mLocale)
        }
    }

    func count() -> Int {
        LanguagesLogic.loadSemaphore.wait()
        LanguagesLogic.loadSemaphore.signal()

        return languages != nil ? languages!.count : 0
    }

    private func loadLanguages() {
        LanguagesLogic.loadSemaphore.wait()

        Tools.executeInGlobalQueue() {
            do {
                try self.repository.getLanguages()
                { (_result, _languages) in
                    if _result == CallbackResult.Success {
                        self.languages = _languages
                    }

                    LanguagesLogic.loadSemaphore.signal()
                }
            }

            catch {
                LanguagesLogic.loadSemaphore.signal()
            }
        }
    }

    func getLanguages(_ callback: @escaping LanguagesCallback) throws {
        LanguagesLogic.loadSemaphore.wait()
        LanguagesLogic.loadSemaphore.signal()

        guard count() > 0 else {
            throw Exception.NoObjectOrEmpty
        }

        callback(.Success, languages!)
    }

    func getCurrentLanguage() throws -> LanguageModel {
        let code = preferences.getCurrentLanguageCode()

        if !code.isEmpty {
            return try getLanguageForCode(code)
        }

        // if it's default (invalid) language get phone's language
        var phoneLanguageCode = ""
        do {
            // do we support phone's language?
            phoneLanguageCode = try locale.getPhoneLanguageCode()
            let phoneLanguage = try getLanguageForCode(phoneLanguageCode)
            // save to properties
            try saveCurrentLanguage(phoneLanguage)

            return phoneLanguage
        } catch {
            print("Language with code \"\(phoneLanguageCode)\" not supported")
        }

        // if we don't support it use english
        let language = try getLanguageForCode("en")
        // save to properties
        try saveCurrentLanguage(language)

        return language
    }

    func saveCurrentLanguage(_ language: LanguageModel) throws {
        try preferences.saveCurrentLanguageCode(language.getCode())
    }

    private func getLanguageForCode(_ code: String) throws -> LanguageModel {
        LanguagesLogic.loadSemaphore.wait()
        LanguagesLogic.loadSemaphore.signal()

        guard count() > 0 else {
            throw Exception.NoObjectOrEmpty
        }

        if code.isEmpty {
            throw Exception.InvalidArgument
        }

        let language = languages!.first{$0.getCode() == code}
        if language == nil {
            throw Exception.NoObjectOrEmpty
        }

        return language!
    }
}
