//
//  MusicLogic.swift
//  Verses
//
//  Created by Darko VUKOTIC on 2.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class MusicLogic: NSObject, OnMusicEndDelegate {
    private var musicFileNames: [String]?
    private var repository: MusicRepository!
    private var adapter: MusicAdapter!
    // used for everybody to wait until load ends
    private static let loadSemaphore = DispatchSemaphore(value: 1)

    private override init() {
        super.init()
    }

    private init(repository _repository: MusicRepository?, adapter _adapter: MusicAdapter?) throws {
        super.init()

        if _repository == nil || _adapter == nil {
            throw Exception.InvalidArgument
        }

        self.repository = _repository!
        self.adapter = _adapter!

        loadMusicFileNames()
    }

    class Builder {
        private var mRepository: MusicRepository?
        private var mAdapter: MusicAdapter?

        func repository(_ _repository: MusicRepository) -> Builder {
            mRepository = _repository
            return self
        }

        func adapter(_ _adapter: MusicAdapter) -> Builder {
            mAdapter = _adapter
            return self
        }

        func build() throws -> MusicLogic {
            return try MusicLogic.init(repository: mRepository, adapter: mAdapter)
        }
    }

    func count() -> Int {
        MusicLogic.loadSemaphore.wait()
        MusicLogic.loadSemaphore.signal()

        return musicFileNames != nil ? musicFileNames!.count : 0
    }

    func play() throws {
        MusicLogic.loadSemaphore.wait()
        MusicLogic.loadSemaphore.signal()

        let random = try getRandomMusicFileName()
        try adapter.play(random)
    }

    func stop() {
        MusicLogic.loadSemaphore.wait()
        MusicLogic.loadSemaphore.signal()

        adapter.stop()
    }

    func isPlaying() -> Bool {
        MusicLogic.loadSemaphore.wait()
        MusicLogic.loadSemaphore.signal()

        return adapter.isPlaying()
    }

    private func loadMusicFileNames() {
        MusicLogic.loadSemaphore.wait()

        Tools.executeInGlobalQueue() {
            do {
                try self.repository.getMusicFileNames()
                { (_result, _musicFileNames) in
                    if _result == CallbackResult.Success {
                        self.musicFileNames = _musicFileNames
                    }

                    MusicLogic.loadSemaphore.signal()
                }
            }

            catch {
                MusicLogic.loadSemaphore.signal()
            }
        }
    }

    private func getRandomMusicFileName() throws -> String {
        MusicLogic.loadSemaphore.wait()
        MusicLogic.loadSemaphore.signal()

        guard count() > 0 else {
            throw Exception.NoObjectOrEmpty
        }

        let size = count()
        let rnd = Tools.getRandomPositiveInt()
        let index = rnd % size

        return try getMusicFileNameByIndex(index)
    }

    private func getMusicFileNameByIndex(_ index: Int) throws -> String {
        MusicLogic.loadSemaphore.wait()
        MusicLogic.loadSemaphore.signal()

        guard count() > 0 else {
            throw Exception.NoObjectOrEmpty
        }

        let size = count()

        if index >= size || index < 0 {
            throw Exception.InvalidArgument
        }

        return musicFileNames![index]
    }

    // OnMusicEndDelegate

    func onMusicEnd() {
        // wait few seconds and play again
        Tools.executeDelayedInMainQueue(
            2*1000,
            {
                do { try self.play() }
                catch {}
            }
        )
    }
}
