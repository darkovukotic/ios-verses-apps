//
//  FontsLogic.swift
//  Verses
//
//  Created by Darko VUKOTIC on 28.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class FontsLogic: NSObject {
    private var fonts: [FontModel]?
    private var repository: FontsRepository!
    // used for everybody to wait until load ends
    private static let loadSemaphore = DispatchSemaphore(value: 1)

    private override init() {
        super.init()
    }

    private init(repository _repository: FontsRepository?) throws {
        super.init()

        if _repository == nil {
            throw Exception.InvalidArgument
        }

        self.repository = _repository!

        loadFonts()
    }

    class Builder {
        private var mRepository: FontsRepository?

        func repository(_ _repository: FontsRepository) -> Builder {
            mRepository = _repository
            return self
        }

        func build() throws -> FontsLogic {
            return try FontsLogic.init(repository: mRepository)
        }
    }

    func count() -> Int {
        FontsLogic.loadSemaphore.wait()
        FontsLogic.loadSemaphore.signal()

        return fonts != nil ? fonts!.count : 0
    }

    private func loadFonts() {
        FontsLogic.loadSemaphore.wait()

        Tools.executeInGlobalQueue() {
            do {
                try self.repository.getFonts()
                { (_result, _fonts) in
                    if _result == CallbackResult.Success {
                        self.fonts = _fonts
                    }

                    FontsLogic.loadSemaphore.signal()
                }
            }

            catch {
                FontsLogic.loadSemaphore.signal()
            }
        }
    }

    func getFontWithCode(_ code: FontCode) throws -> FontModel {
        FontsLogic.loadSemaphore.wait()
        FontsLogic.loadSemaphore.signal()

        guard count() > 0 else {
            throw Exception.NoObjectOrEmpty
        }

        let font = fonts!.first{$0.getCode() == code}
        if font == nil {
            throw Exception.NoObjectOrEmpty
        }

        return font!
    }
}
