//
//  WallpapersLogic.swift
//  Verses
//
//  Created by Darko VUKOTIC on 23.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

typealias WallpaperImageFileNamesCallback = (CallbackResult, [String]) -> Void

class WallpapersLogic: NSObject {
    private var wallpapers: [WallpaperModel]?
    private var previousRandomWallpaperIndex = -1
    private var repository: WallpapersRepository!
    private var preferences: PreferencesAdapter!
    // used for everybody to wait until load ends
    private static let loadSemaphore = DispatchSemaphore(value: 1)

    private override init() {
        super.init()
    }

    private init(repository _repository: WallpapersRepository?, preferences _preferences: PreferencesAdapter?) throws {
        super.init()

        if _repository == nil || _preferences == nil {
            throw Exception.InvalidArgument
        }

        self.repository = _repository!
        self.preferences = _preferences!

        loadWallpapers()
    }

    class Builder {
        private var mRepository: WallpapersRepository?
        private var mPreferences: PreferencesAdapter?

        func repository(_ _repository: WallpapersRepository) -> Builder {
            mRepository = _repository
            return self
        }

        func preferences(_ _preferences: PreferencesAdapter) -> Builder {
            mPreferences = _preferences
            return self
        }

        func build() throws -> WallpapersLogic {
            return try WallpapersLogic.init(repository: mRepository, preferences: mPreferences)
        }
    }

    func count() -> Int {
        WallpapersLogic.loadSemaphore.wait()
        WallpapersLogic.loadSemaphore.signal()

        return wallpapers != nil ? wallpapers!.count : 0
    }

    private func loadWallpapers() {
        WallpapersLogic.loadSemaphore.wait()

        Tools.executeInGlobalQueue() {
            do {
                try self.repository.getWallpapers()
                    { (_result, _wallpapers) in
                        if _result == CallbackResult.Success {
                            self.wallpapers = _wallpapers
                        }

                        WallpapersLogic.loadSemaphore.signal()
                    }
            }

            catch {
                WallpapersLogic.loadSemaphore.signal()
            }
        }
    }

    private func extractFileNames() -> [String] {
        return self.wallpapers!.map{$0.getFileName()}
    }

    func getWallpaperImageFileNames(_ callback: @escaping WallpaperImageFileNamesCallback) throws {
        WallpapersLogic.loadSemaphore.wait()
        WallpapersLogic.loadSemaphore.signal()

        guard count() > 0 else {
            throw Exception.NoObjectOrEmpty
        }

        callback(.Success, extractFileNames())
    }

    func getRandomWallpaper() throws -> WallpaperModel {
        WallpapersLogic.loadSemaphore.wait()
        WallpapersLogic.loadSemaphore.signal()

        guard count() > 0 else {
            throw Exception.NoObjectOrEmpty
        }

        let size = count()

        var rnd = Tools.getRandomPositiveInt()
        var index = rnd % size

        // try one more time
        if previousRandomWallpaperIndex == index {
            rnd = Tools.getRandomPositiveInt()
            index = rnd % size

            // and one more time
            if previousRandomWallpaperIndex == index {
                rnd = Tools.getRandomPositiveInt()
                index = rnd % size
            }
        }

        previousRandomWallpaperIndex = index

        return try getWallpaperByIndex(index)
    }

    func getWallpaperByIndex(_ index: Int) throws -> WallpaperModel {
        WallpapersLogic.loadSemaphore.wait()
        WallpapersLogic.loadSemaphore.signal()

        guard count() > 0 else {
            throw Exception.NoObjectOrEmpty
        }

        let size = count()

        if index >= size || index < 0 {
            throw Exception.InvalidArgument
        }

        return wallpapers![index]
    }

    func getLastSelectedWallpaperIndex() throws -> Int {
        WallpapersLogic.loadSemaphore.wait()
        WallpapersLogic.loadSemaphore.signal()

        guard count() > 0 else {
            throw Exception.NoObjectOrEmpty
        }

        let size = count()

        var index = preferences.getLastSelectedWallpaperIndex()

        if index < 0 {
            throw Exception.ForbiddenCode
        }

        // maybe app version has changed
        if index >= size {
            index = 0
            try preferences.saveLastSelectedWallpaperIndex(index)
        }

        return index
    }

    func saveLastSelectedWallpaperIndex(_ index: Int) throws {
        WallpapersLogic.loadSemaphore.wait()
        WallpapersLogic.loadSemaphore.signal()

        guard count() > 0 else {
            throw Exception.NoObjectOrEmpty
        }

        let size = count()

        if index >= size || index < 0 {
            throw Exception.InvalidArgument
        }

        try preferences.saveLastSelectedWallpaperIndex(index)
    }

}
