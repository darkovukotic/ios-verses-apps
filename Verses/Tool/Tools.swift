//
//  Tools.swift
//  Verses
//
//  Created by Darko VUKOTIC on 22.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class Tools: NSObject {
    private override init() {
        super.init()
    }

    static func getRandomPositiveInt() -> Int {
        let rnd = Int(arc4random() % UInt32(Int32.max))
        return rnd < 0 ? rnd * -1 : rnd
    }

    static func executeInMainQueue(_ code: @escaping ()->Void) {
        DispatchQueue.main.async {
            code()
        }
    }

    static func executeDelayedInMainQueue(_ timeInMS: Int, _ code: @escaping ()->Void) {
        let when = DispatchTime.now() + .milliseconds(timeInMS)

        DispatchQueue.main.asyncAfter(deadline: when) {
            code()
        }
    }

    static func executeInGlobalQueue(_ code: @escaping ()->Void) {
        DispatchQueue.global().async {
            code()
        }
    }

    static func executeDelayedInGlobalQueue(_ timeInMS: Int, _ code: @escaping ()->Void) {
        let when = DispatchTime.now() + .milliseconds(timeInMS)

        DispatchQueue.global().asyncAfter(deadline: when) {
            code()
        }
    }

    static func getAppVersion() throws -> String {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        else {
            throw Exception.NoObjectOrEmpty
        }
    }

    static func getCurrentYear() throws -> String {
        return String(Calendar.current.component(.year, from: Date()))
    }

    static func sleep(_ timeMs: Int) {
        usleep(1000 * UInt32(timeMs))
    }
}
