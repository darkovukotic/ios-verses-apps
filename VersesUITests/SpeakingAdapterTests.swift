//
//  SpeakingAdapterTests.swift
//  VersesUITests
//
//  Created by Darko VUKOTIC on 1.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class SpeakingAdapterTests: XCTestCase {
    var adapter: SpeakingAdapter!
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.

        do {
            adapter = try Provider.instance.provideSpeakingAdapter()
        } catch {
            XCTFail("Failed to get adapter")
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testShouldntSpeakOnTheStart() throws {
        XCTAssertFalse(adapter.isSpeaking())
    }

    func testIsLanguageCodeSupportedShouldFailOnEmptyString() throws {
        do {
            _ = try adapter.isLanguageCodeSupported("")
            XCTFail("It should fail")
        } catch Exception.InvalidArgument {
            // pass
        } catch {
            XCTFail("Unexpected error has happened")
        }
    }
    
    func testSupportedLanguages() throws {
        XCTAssertTrue(try adapter.isLanguageCodeSupported("en"))
        XCTAssertFalse(try adapter.isLanguageCodeSupported("sr"))
    }

    func testStartShouldFailOnEmptyText() throws {
        do {
            try adapter.start(text: "", languageCode: "en")
            XCTFail("It should fail")
        } catch Exception.InvalidArgument {
            // pass
        } catch {
            XCTFail("Unexpected error has happened")
        }
    }

    func testStartShouldFailOnEmptyLanguageCode() throws {
        do {
            try adapter.start(text: "La la la", languageCode: "")
            XCTFail("It should fail")
        } catch Exception.InvalidArgument {
            // pass
        } catch {
            XCTFail("Unexpected error has happened")
        }
    }

    func testStartShouldFailOnUnsupportedLanguageCode() throws {
        do {
            try adapter.start(text: "La la la", languageCode: "sr")
            XCTFail("It should fail")
        } catch Exception.CantStartSpeaking {
            // pass
        } catch {
            XCTFail("Unexpected error has happened")
        }
    }

    func testIsSpeakingShouldWorkBeforeDuringAndAfterSpeaking() throws {
        XCTAssertFalse(adapter.isSpeaking())

        try adapter.start(
            text: "La da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka",
            languageCode: "en"
        )

        Tools.sleep(500)

        XCTAssertTrue(adapter.isSpeaking())

        Tools.sleep(8 * 1000)

        XCTAssertFalse(adapter.isSpeaking())
    }

    func testStopShouldWork() throws {
        XCTAssertFalse(adapter.isSpeaking())

        try adapter.start(
            text: "La da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka",
            languageCode: "en"
        )

        Tools.sleep(500)

        XCTAssertTrue(adapter.isSpeaking())

        Tools.sleep(2 * 1000)

        XCTAssertTrue(adapter.isSpeaking())

        adapter.stop()

        Tools.sleep(500)

        XCTAssertFalse(adapter.isSpeaking())
    }

    func testStartStopStartStopShouldWork() throws {
        let text = "La da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka"

        let iteration: () throws -> () = {
            XCTAssertFalse(self.adapter.isSpeaking())

            try self.adapter.start(
                text: text,
                languageCode: "en"
            )

            Tools.sleep(500)
            XCTAssertTrue(self.adapter.isSpeaking())

            Tools.sleep(2 * 1000)
            XCTAssertTrue(self.adapter.isSpeaking())

            self.adapter.stop()

            Tools.sleep(500)
            XCTAssertFalse(self.adapter.isSpeaking())
        }

        try iteration()
        try iteration()
        try iteration()
    }

    func testSeveralStopsShouldntCrashIt() throws {
        let text = "La da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka"

        XCTAssertFalse(self.adapter.isSpeaking())

        try self.adapter.start(
            text: text,
            languageCode: "en"
        )

        Tools.sleep(500)
        XCTAssertTrue(self.adapter.isSpeaking())

        Tools.sleep(2 * 1000)
        XCTAssertTrue(self.adapter.isSpeaking())

        self.adapter.stop()

        Tools.sleep(500)
        XCTAssertFalse(self.adapter.isSpeaking())

        self.adapter.stop()
        self.adapter.stop()
        self.adapter.stop()

        Tools.sleep(500)
        XCTAssertFalse(self.adapter.isSpeaking())
    }

    func testSeveralStartsShouldStopCurrentAndStartNewOne() throws {
        let text = "La da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka ve be ze pe da fa ga ka"

        XCTAssertFalse(self.adapter.isSpeaking())

        let iteration: () throws -> () = {
            try self.adapter.start(
                text: text,
                languageCode: "en"
            )

            Tools.sleep(500)
            XCTAssertTrue(self.adapter.isSpeaking())

            Tools.sleep(2 * 1000)
            XCTAssertTrue(self.adapter.isSpeaking())
        }

        try iteration()
        try iteration()
        try iteration()
    }

}
