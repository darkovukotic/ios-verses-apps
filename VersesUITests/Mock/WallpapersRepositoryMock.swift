//
//  WallpapersRepositoryMock.swift
//  Verses
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class WallpapersRepositoryMock: NSObject, WallpapersRepository {

    func getWallpapers(_ callback: @escaping WallpapersCallback) throws {
        Tools.executeDelayedInGlobalQueue(500) {
            do {
                let wallpapers = try [WallpaperModel("image1"), WallpaperModel("image2"), WallpaperModel("image3")]
                callback(.Success, wallpapers)

            } catch {
                callback(.Fail, [WallpaperModel]())
            }
        }
    }

}
