//
//  VersesRepositoryMock.swift
//  Verses
//
//  Created by Darko VUKOTIC on 23.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class VersesRepositoryMock: NSObject, VersesRepository {
    func getVerses(type: VersesType, _ callback: @escaping VersesCallback) throws {
        switch type {
            case .Random:
                try getRandomVerses(callback)

            case .Next:
                try getNextVerses(callback)

            case .Current:
                throw Exception.ForbiddenCode
        }
    }

    private func getNextVerses(_ callback: @escaping VersesCallback) throws {
        Tools.executeDelayedInGlobalQueue(500) {
            do {
                let verses = try VersesModel("Some next verses Some next verses Some next verses")
                callback(.Success, .Next, verses)

            } catch {
                callback(.Fail, .Next, VersesModel.Empty)
            }
        }
    }

    private func getRandomVerses(_ callback: @escaping VersesCallback) throws {
        Tools.executeDelayedInGlobalQueue(500) {
            do {
                let verses = try VersesModel("Some random verses Some random verses Some random verses")
                callback(.Success, .Random, verses)

            } catch {
                callback(.Fail, .Random, VersesModel.Empty)
            }
        }
    }
}
