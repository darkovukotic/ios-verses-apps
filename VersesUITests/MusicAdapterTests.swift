//
//  MusicAdapterTests.swift
//  VersesUITests
//
//  Created by Darko VUKOTIC on 5.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class MusicAdapterTests: XCTestCase {
    var adapter: MusicAdapter!
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.

        adapter = MusicAdapterObject()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.

        adapter.stop()

        super.tearDown()
    }
    
    func testThereShouldntBeMusicOnTheStart() throws {
        XCTAssertFalse(adapter.isPlaying())
    }

    func testPlayShouldFailOnEmptyName() throws {
        do {
            try adapter.play("")
            XCTFail("It should fail")
        } catch Exception.InvalidArgument {
            // pass
        } catch {
            XCTFail("Unexpected error has happened")
        }
    }

    func testIsPlayingShouldWorkBeforeDuringAndAfterPlaying() throws {
        XCTAssertFalse(adapter.isPlaying())

        try adapter.play("azan")

        Tools.sleep(500)
        XCTAssertTrue(adapter.isPlaying())

        Tools.sleep(4 * 1000)
        adapter.stop()

        Tools.sleep(500)
        XCTAssertFalse(adapter.isPlaying())
    }

    func testPlayStopPlayStopShouldWork() throws {
        let iteration: () throws -> () = {
            XCTAssertFalse(self.adapter.isPlaying())

            try self.adapter.play("azan")

            Tools.sleep(2 * 1000)
            XCTAssertTrue(self.adapter.isPlaying())

            self.adapter.stop()

            Tools.sleep(500)
            XCTAssertFalse(self.adapter.isPlaying())
        }

        try iteration()
        try iteration()
        try iteration()
    }

    func testSeveralStopsShouldntCrashIt() throws {
        XCTAssertFalse(self.adapter.isPlaying())

        try self.adapter.play("azan")

        Tools.sleep(2 * 1000)
        XCTAssertTrue(self.adapter.isPlaying())

        self.adapter.stop()

        Tools.sleep(500)
        XCTAssertFalse(self.adapter.isPlaying())

        self.adapter.stop()
        self.adapter.stop()
        self.adapter.stop()

        Tools.sleep(500)
        XCTAssertFalse(self.adapter.isPlaying())
    }

    func testSeveralPlaysShouldStopCurrentAndStartNewOne() throws {
        XCTAssertFalse(self.adapter.isPlaying())

        let iteration: () throws -> () = {
            try self.adapter.play("azan")
            Tools.sleep(3 * 1000)
            XCTAssertTrue(self.adapter.isPlaying())
        }

        try iteration()
        try iteration()
        try iteration()
    }
    
}
