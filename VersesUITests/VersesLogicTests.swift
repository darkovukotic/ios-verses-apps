//
//  VersesLogicTests.swift
//  VersesUITests
//
//  Created by Darko VUKOTIC on 23.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class VersesLogicTests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
//        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
//        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testShouldReturnRandomVerses() throws {
        let verses = try VersesLogic.Builder()
            .repository(VersesRepositoryMock())
            .build()

        let vExpectation = expectation(description: "getVerses() callback")

        try verses.getVerses(type: .Random)
            {(result, type, versesObj) in
                XCTAssertNotNil(versesObj)
                XCTAssertFalse(versesObj.getVersesStr().isEmpty)
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(VersesType.Random, type)

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 1.5) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }

    func testShouldReturnNextVerses() throws {
        let verses = try VersesLogic.Builder()
            .repository(VersesRepositoryMock())
            .build()

        let vExpectation = expectation(description: "getVerses() callback")

        try verses.getVerses(type: .Next)
            {(result, type, versesObj) in
                XCTAssertNotNil(versesObj)
                XCTAssertFalse(versesObj.getVersesStr().isEmpty)
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(VersesType.Next, type)

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 1.5) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }

    func testShouldFailOnFirstCallOfCurrentVerses() throws {
        let verses = try VersesLogic.Builder()
            .repository(VersesRepositoryMock())
            .build()

        let vExpectation = expectation(description: "getVerses() callback")

        try verses.getVerses(type: .Current)
            {(result, type, versesObj) in
                XCTAssertNotNil(versesObj)
                XCTAssertFalse(versesObj.getVersesStr().isEmpty)
                XCTAssertEqual(CallbackResult.Fail, result)
                XCTAssertEqual(VersesType.Current, type)

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 1.5) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }

    func testShouldReturnCurrentVersesAsSecondCallAfterRandomVerses() throws {
        let verses = try VersesLogic.Builder()
            .repository(VersesRepositoryMock())
            .build()

        // first call random verses
        try verses.getVerses(
            type: .Random,
            {(_, _, _) in }
        )

        sleep(1)

        let vExpectation = expectation(description: "getVerses() callback")

        try verses.getVerses(type: .Current)
            {(result, type, versesObj) in
                XCTAssertNotNil(versesObj)
                XCTAssertFalse(versesObj.getVersesStr().isEmpty)
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(VersesType.Current, type)
                XCTAssertEqual(versesObj.getVersesStr(), "Some random verses Some random verses Some random verses")

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 1.5) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }

    func testShouldReturnCurrentVersesAsSecondCallAfterNextVerses() throws {
        let verses = try VersesLogic.Builder()
            .repository(VersesRepositoryMock())
            .build()

        // first call random verses
        try verses.getVerses(
            type: .Next,
            {(_, _, _) in }
        )

        sleep(1)

        let vExpectation = expectation(description: "getVerses() callback")

        try verses.getVerses(type: .Current)
            {(result, type, versesObj) in
                XCTAssertNotNil(versesObj)
                XCTAssertFalse(versesObj.getVersesStr().isEmpty)
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(VersesType.Current, type)
                XCTAssertEqual(versesObj.getVersesStr(), "Some next verses Some next verses Some next verses")

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 1.5) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }
    
}
