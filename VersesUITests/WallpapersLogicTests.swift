//
//  WallpapersLogicTests.swift
//  VersesUITests
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class WallpapersLogicTests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
//        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
//        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testShouldProvideWallpaperImageFileNames() throws {
        let wallpapers = try WallpapersLogic.Builder()
            .repository(WallpapersRepositoryMock())
            .preferences(PreferencesMock_NoWait())
            .build()

        let vExpectation = expectation(description: "getWallpaperImageFileNames() callback")

        try wallpapers.getWallpaperImageFileNames()
            { (result, fileNames) in
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(3, fileNames.count)
                XCTAssertEqual("image1", fileNames[0])
                XCTAssertEqual("image2", fileNames[1])
                XCTAssertEqual("image3", fileNames[2])

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 1.5) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }

    func testShouldSucceedToGetCachedWallpaperImageFileNamesWithoutWaitingButAfterOneWaitingCall() throws {
        let wallpapers = try WallpapersLogic.Builder()
            .repository(WallpapersRepositoryMock())
            .preferences(PreferencesMock_NoWait())
            .build()

        // get it out of cache so we have to wait
        let vExpectation1 = expectation(description: "getWallpaperImageFileNames() callback")

        try wallpapers.getWallpaperImageFileNames()
            { (result, fileNames) in
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(3, fileNames.count)
                XCTAssertEqual("image1", fileNames[0])
                XCTAssertEqual("image2", fileNames[1])
                XCTAssertEqual("image3", fileNames[2])

                vExpectation1.fulfill()
            }

        wait(for: [vExpectation1], timeout: 1)

        // now get it from cache without waiting
        let vExpectation2 = expectation(description: "cached getWallpaperImageFileNames() callback")

        try wallpapers.getWallpaperImageFileNames()
            { (result, fileNames) in
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(3, fileNames.count)
                XCTAssertEqual("image1", fileNames[0])
                XCTAssertEqual("image2", fileNames[1])
                XCTAssertEqual("image3", fileNames[2])

                vExpectation2.fulfill()
            }

        wait(for: [vExpectation2], timeout: 0)
    }
    
}
