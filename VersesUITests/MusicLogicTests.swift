//
//  MusicLogicTests.swift
//  VersesUITests
//
//  Created by Darko VUKOTIC on 5.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class MusicLogicTests: XCTestCase {
    var logic: MusicLogic!
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.

        do {
            logic = try MusicLogic.Builder()
                .adapter(MusicAdapterObject())
                .repository(MusicRepositoryMock_NoWait())
                .build()
        } catch {}
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.

        logic.stop()

        super.tearDown()
    }
    
    func testThereShouldntBeMusicOnTheStart() throws {
        XCTAssertFalse(logic.isPlaying())
    }

    func testIsPlayingShouldWorkBeforeDuringAndAfterPlaying() throws {
        XCTAssertFalse(logic.isPlaying())

        try logic.play()

        Tools.sleep(500)
        XCTAssertTrue(logic.isPlaying())

        Tools.sleep(4 * 1000)
        logic.stop()

        Tools.sleep(500)
        XCTAssertFalse(logic.isPlaying())
    }

    func testPlayStopPlayStopShouldWork() throws {
        let iteration: () throws -> () = {
            XCTAssertFalse(self.logic.isPlaying())

            try self.logic.play()

            Tools.sleep(4 * 1000)
            XCTAssertTrue(self.logic.isPlaying())

            self.logic.stop()

            Tools.sleep(500)
            XCTAssertFalse(self.logic.isPlaying())
        }

        try iteration()
        try iteration()
        try iteration()
    }

    func testSeveralStopsShouldntCrashIt() throws {
        XCTAssertFalse(self.logic.isPlaying())

        try self.logic.play()

        Tools.sleep(2 * 1000)
        XCTAssertTrue(self.logic.isPlaying())

        self.logic.stop()

        Tools.sleep(500)
        XCTAssertFalse(self.logic.isPlaying())

        self.logic.stop()
        self.logic.stop()
        self.logic.stop()

        Tools.sleep(500)
        XCTAssertFalse(self.logic.isPlaying())
    }

    func testSeveralPlaysShouldStopCurrentAndStartNewOne() throws {
        XCTAssertFalse(self.logic.isPlaying())

        let iteration: () throws -> () = {
            try self.logic.play()
            Tools.sleep(5 * 1000)
            XCTAssertTrue(self.logic.isPlaying())
        }

        try iteration()
        try iteration()
        try iteration()
    }
    
}
