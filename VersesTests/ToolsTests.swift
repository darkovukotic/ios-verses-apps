//
//  ToolsTests.swift
//  VersesTests
//
//  Created by Darko VUKOTIC on 22.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class ToolsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetRandomPositiveIntMostOfTheTimeShouldReturnDifferentValues() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.

        let int1 = Tools.getRandomPositiveInt()
        let int2 = Tools.getRandomPositiveInt()
        let int3 = Tools.getRandomPositiveInt()

        XCTAssertNotEqual(int1, int2)
        XCTAssertNotEqual(int1, int3)
        XCTAssertNotEqual(int3, int2)
    }

    func testGetRandomPositiveIntMostOfTheTimeShouldReturnPositiveValues() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.

        let int1 = Tools.getRandomPositiveInt()
        let int2 = Tools.getRandomPositiveInt()
        let int3 = Tools.getRandomPositiveInt()

        XCTAssertTrue(int1 >= 0)
        XCTAssertTrue(int2 >= 0)
        XCTAssertTrue(int3 >= 0)
    }

    func testGetAppVersion() throws {
        let ver = try Tools.getAppVersion()
        XCTAssertFalse(ver.isEmpty)
    }

    func testGetCurrentYear() throws {
        let year = try Tools.getCurrentYear()
        XCTAssertTrue(year >= "2018")
    }
}
