//
//  LanguagesLogicTests.swift
//  VersesTests
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class LanguagesLogicTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLanguageShouldHaveNameCodeAndFont() throws {
        let lang = try LanguageModel(code:"tr", name:"Türkçe", fontCode:FontCode.LatinFont)

        XCTAssertEqual(lang.getCode(), "tr")
        XCTAssertEqual(lang.getName(), "Türkçe")
        XCTAssertEqual(lang.getPreferredFontCode(), FontCode.LatinFont)
    }

    func testShouldHaveLoadedLanguagesOnTheStart() throws {
        let languages = try LanguagesLogic.Builder()
            .repository(LanguagesRepositoryMock_NoWait())
            .locale(LocaleMock_NoWait())
            .preferences(PreferencesMock_NoWait())
            .build()

        XCTAssertEqual(5, languages.count())
    }

    func testShouldReturnLanguageArray() throws {
        let languages = try LanguagesLogic.Builder()
            .repository(LanguagesRepositoryMock_NoWait())
            .locale(LocaleMock_NoWait())
            .preferences(PreferencesMock_NoWait())
            .build()

        let vExpectation = expectation(description: "getLanguages() callback")

        try languages.getLanguages()
            { (result, languageArray) in
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(5, languageArray.count)

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 0.1)
    }

    func testShouldGetCurrentLanguage() throws {
        let languages = try LanguagesLogic.Builder()
            .repository(LanguagesRepositoryMock_NoWait())
            .locale(LocaleMock_NoWait())
            .preferences(PreferencesMock_NoWait())
            .build()

        try languages.saveCurrentLanguage(LanguageModel(code:"ar", name:"Arabian", fontCode:.LatinFont))

        let lang = try languages.getCurrentLanguage()

        XCTAssertEqual("ar", lang.getCode())
    }
    
}
