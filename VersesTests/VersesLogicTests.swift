//
//  VersesLogicTests.swift
//  VersesTests
//
//  Created by Darko VUKOTIC on 21.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class VersesLogicTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testBuilderShouldFailWithoutRepository() throws {
        do {
            _ = try VersesLogic.Builder()
            .build()
            XCTFail("Exception expected")
        }
        catch Exception.InvalidArgument {
            // pass
        }
        catch {
            XCTFail("InvalidArgument exception expected")
        }
    }
    
    func testBuilderShouldWork() throws {
        let verses = try VersesLogic.Builder()
            .repository(VersesRepositoryMock_NoWait())
            .build()
        XCTAssertNotNil(verses)
    }

    func testShouldReturnRandomVerses() throws {
        let verses = try VersesLogic.Builder()
            .repository(VersesRepositoryMock_NoWait())
            .build()

        let vExpectation = expectation(description: "getVerses() callback")

        try verses.getVerses(type: .Random)
            {(result, type, versesObj) in
                XCTAssertNotNil(versesObj)
                XCTAssertFalse(versesObj.getVersesStr().isEmpty)
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(VersesType.Random, type)

                vExpectation.fulfill()
            }


        waitForExpectations(timeout: 0.1) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }

    func testShouldReturnNextVerses() throws {
        let verses = try VersesLogic.Builder()
            .repository(VersesRepositoryMock_NoWait())
            .build()

        let vExpectation = expectation(description: "getVerses() callback")

        try verses.getVerses(type: .Next)
            {(result, type, versesObj) in
                XCTAssertNotNil(versesObj)
                XCTAssertFalse(versesObj.getVersesStr().isEmpty)
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(VersesType.Next, type)

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 0.1) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }

    func testShouldFailOnFirstCallOfCurrentVerses() throws {
        let verses = try VersesLogic.Builder()
            .repository(VersesRepositoryMock_NoWait())
            .build()

        let vExpectation = expectation(description: "getVerses() callback")

        try verses.getVerses(type: .Current)
            {(result, type, versesObj) in
                XCTAssertNotNil(versesObj)
                XCTAssertFalse(versesObj.getVersesStr().isEmpty)
                XCTAssertEqual(CallbackResult.Fail, result)
                XCTAssertEqual(VersesType.Current, type)

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 0.1) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }

    func testShouldReturnCurrentVersesAsSecondCallAfterRandomVerses() throws {
        let verses = try VersesLogic.Builder()
            .repository(VersesRepositoryMock_NoWait())
            .build()

        // first call random verses
        try verses.getVerses(
            type: .Random,
            {(_, _, _) in }
        )

        let vExpectation = expectation(description: "getVerses() callback")

        try verses.getVerses(type: .Current)
            {(result, type, versesObj) in
                XCTAssertNotNil(versesObj)
                XCTAssertFalse(versesObj.getVersesStr().isEmpty)
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(VersesType.Current, type)
                XCTAssertEqual(versesObj.getVersesStr(), "Some random verses Some random verses Some random verses")

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 0.1) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }

    func testShouldReturnCurrentVersesAsSecondCallAfterNextVerses() throws {
        let verses = try VersesLogic.Builder()
            .repository(VersesRepositoryMock_NoWait())
            .build()

        // first call random verses
        try verses.getVerses(
            type: .Next,
            {(_, _, _) in }
        )

        let vExpectation = expectation(description: "getVerses() callback")

        try verses.getVerses(type: .Current)
            {(result, type, versesObj) in
                XCTAssertNotNil(versesObj)
                XCTAssertFalse(versesObj.getVersesStr().isEmpty)
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(VersesType.Current, type)
                XCTAssertEqual(versesObj.getVersesStr(), "Some next verses Some next verses Some next verses")

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 0.1) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
