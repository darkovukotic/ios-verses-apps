//
//  NavigationPanePresenterTests.swift
//  VersesTests
//
//  Created by Darko VUKOTIC on 15.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class NavigationPanePresenterTests: XCTestCase {
    private var presenter: NavigationPanePresenter!
    private var music: MusicLogic!
    private let preferences = PreferencesAdapterObject()
    private var wallpapers: WallpapersLogic!
    private let speaking = SpeakingAdapterObject()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        do {
            music = try MusicLogic.Builder()
                .adapter(MusicAdapterObject())
                .repository(MusicRepositoryMock_NoWait())
                .build()

            wallpapers = try WallpapersLogic.Builder()
                .preferences(preferences)
                .repository(WallpapersRepositoryMock())
                .build()

        } catch {
            // unfortunately it actually doesn't fail the test
            XCTFail()
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        music.stop()

        super.tearDown()
    }
    
    func testOnAppStartedShouldStartMusicWhenEnabled() throws {
        // must be here because presenter holds it as weak
        let view = NavigationPaneViewMock()

        presenter = try NavigationPanePresenterObject.Builder()
            .music(music)
            .preferences(preferences)
            .speaking(speaking)
            .wallpapers(wallpapers)
            .view(view)
            .build()

        try preferences.saveIsMusicEnabled(true)

        presenter.onAppActive()
        XCTAssertTrue(music.isPlaying())
    }

    func testOnAppStartedShouldNotStartMusicWhenNotEnabled() throws {
        // must be here because presenter holds it as weak
        let view = NavigationPaneViewMock()

        presenter = try NavigationPanePresenterObject.Builder()
            .music(music)
            .preferences(preferences)
            .speaking(speaking)
            .wallpapers(wallpapers)
            .view(view)
            .build()

        try preferences.saveIsMusicEnabled(false)

        presenter.onAppActive()
        XCTAssertFalse(music.isPlaying())
    }

    func testOnAppStartedShouldSetBackgroundImage() throws {

        class NavigationPaneViewMock_checksSetBackgroundImage : NavigationPaneViewMock {
            var expectation: XCTestExpectation

            init(_ _expectation: XCTestExpectation) {
                self.expectation = _expectation
            }

            override func setBackgroundImage(_ imageFileName: String) {
                if !imageFileName.isEmpty {
                    self.expectation.fulfill()
                }
            }
        }

        let view = NavigationPaneViewMock_checksSetBackgroundImage(expectation(description: "view's setBackgroundImage()"))

        presenter = try NavigationPanePresenterObject.Builder()
            .music(music)
            .preferences(preferences)
            .speaking(speaking)
            .wallpapers(wallpapers)
            .view(view)
            .build()

        presenter.onAppActive()

        waitForExpectations(timeout: 1) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }

    func testOnAppStopShouldStopMusic() throws {
        // must be here because presenter holds it as weak
        let view = NavigationPaneViewMock()

        presenter = try NavigationPanePresenterObject.Builder()
            .music(music)
            .preferences(preferences)
            .speaking(speaking)
            .wallpapers(wallpapers)
            .view(view)
            .build()

        try preferences.saveIsMusicEnabled(true)

        presenter.onAppActive()
        XCTAssertTrue(music.isPlaying())

        presenter.onAppInactive()
        XCTAssertFalse(music.isPlaying())
    }

    func testOnAppStopShouldStopSpeaking() throws {
        // must be here because presenter holds it as weak
        let view = NavigationPaneViewMock()

        presenter = try NavigationPanePresenterObject.Builder()
            .music(music)
            .preferences(preferences)
            .speaking(speaking)
            .wallpapers(wallpapers)
            .view(view)
            .build()

        try speaking.start(text: "La la la", languageCode: "en")
        Tools.sleep(300)
        XCTAssertTrue(speaking.isSpeaking())

        presenter.onAppInactive()
        XCTAssertFalse(speaking.isSpeaking())
    }
}
