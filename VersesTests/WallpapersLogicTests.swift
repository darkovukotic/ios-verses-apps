//
//  WallpapersLogicTests.swift
//  VersesTests
//
//  Created by Darko VUKOTIC on 23.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit
import XCTest

class WallpapersLogicTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testWallpaperHasFileName() throws {
        let image = try? WallpaperModel("image1")

        XCTAssertNotNil(image)
        XCTAssertEqual(image!.getFileName(), "image1")
    }

    func testThrowsExceptionOnEmptyFileName() throws {
        do {
            _ = try WallpaperModel("")
            XCTFail("Exception not thrown")
        } catch Exception.InvalidArgument {
            // passed
        } catch {
            XCTFail("Some other exception has been thrown")
        }
    }

    func testShouldHave3Wallpapers() throws {
        let wallpapers = try WallpapersLogic.Builder()
            .repository(WallpapersRepositoryMock_NoWait())
            .preferences(PreferencesMock_NoWait())
            .build()

        XCTAssertEqual(3, wallpapers.count())
    }

    func testShouldProvideWallpaperImageFileNames() throws {
        let wallpapers = try WallpapersLogic.Builder()
            .repository(WallpapersRepositoryMock_NoWait())
            .preferences(PreferencesMock_NoWait())
            .build()

        let vExpectation = expectation(description: "getWallpaperImageFileNames() callback")

        try wallpapers.getWallpaperImageFileNames()
            { (result, fileNames) in
                XCTAssertEqual(CallbackResult.Success, result)
                XCTAssertEqual(3, fileNames.count)
                XCTAssertEqual("image1", fileNames[0])
                XCTAssertEqual("image2", fileNames[1])
                XCTAssertEqual("image3", fileNames[2])

                vExpectation.fulfill()
            }

        waitForExpectations(timeout: 0.1) { (error) in
            if let error = error {
                XCTFail("\(error)")
            }
        }
    }

    func testGetWallpaperByIndex() throws {
        let wallpapers = try WallpapersLogic.Builder()
            .repository(WallpapersRepositoryMock_NoWait())
            .preferences(PreferencesMock_NoWait())
            .build()

        var wallpaper = try wallpapers.getWallpaperByIndex(0)
        XCTAssertEqual("image1", wallpaper.getFileName())

        wallpaper = try wallpapers.getWallpaperByIndex(1)
        XCTAssertEqual("image2", wallpaper.getFileName())

        wallpaper = try wallpapers.getWallpaperByIndex(2)
        XCTAssertEqual("image3", wallpaper.getFileName())
    }

    func testGetWallpaperByNegativeIndexShouldFail() throws {
        let wallpapers = try WallpapersLogic.Builder()
            .repository(WallpapersRepositoryMock_NoWait())
            .preferences(PreferencesMock_NoWait())
            .build()

        do {
            _ = try wallpapers.getWallpaperByIndex(-1)
            XCTFail("Exception not thrown")
        } catch Exception.InvalidArgument {
            // passed
        } catch {
            XCTFail("Some other exception has been thrown")
        }
    }

    func testGetWallpaperByTooBigIndexShouldFail() throws {
        let wallpapers = try WallpapersLogic.Builder()
            .repository(WallpapersRepositoryMock_NoWait())
            .preferences(PreferencesMock_NoWait())
            .build()

        do {
            _ = try wallpapers.getWallpaperByIndex(3)
            XCTFail("Exception not thrown")
        } catch Exception.InvalidArgument {
            // passed
        } catch {
            XCTFail("Some other exception has been thrown")
        }
    }

    func testGetLastSelectedWallpaperIndex() throws {
        let wallpapers = try WallpapersLogic.Builder()
            .repository(WallpapersRepositoryMock_NoWait())
            .preferences(PreferencesMock_NoWait())
            .build()

        let index = try wallpapers.getLastSelectedWallpaperIndex()
        XCTAssertEqual(0, index)
    }

    func testGetRandomWallpaper() throws {
        let wallpapers = try WallpapersLogic.Builder()
            .repository(WallpapersRepositoryMock_NoWait())
            .preferences(PreferencesMock_NoWait())
            .build()

        let wp1 = try wallpapers.getRandomWallpaper()
        let wp2 = try wallpapers.getRandomWallpaper()
        let wp3 = try wallpapers.getRandomWallpaper()

        XCTAssertTrue(wp1.getFileName() != wp2.getFileName() || wp1.getFileName() != wp3.getFileName() || wp2.getFileName() != wp3.getFileName())
    }
}
