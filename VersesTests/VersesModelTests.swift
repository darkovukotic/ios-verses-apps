//
//  VersesModelTests.swift
//  VersesTests
//
//  Created by Darko VUKOTIC on 21.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class VersesModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testHasString() {
        let verses = try? VersesModel("Some verses")

        XCTAssertNotNil(verses)
        XCTAssertEqual(verses!.getVersesStr(), "Some verses")
    }

    func testThrowsExceptionOnEmptyString() {
        do {
            _ = try VersesModel("")
            XCTFail("Exception not thrown")
        } catch Exception.InvalidArgument {
            // passed
        } catch {
            XCTFail("Some other exception has been thrown")
        }

    }
    

    
}
