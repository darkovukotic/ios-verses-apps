//
//  MusicRepositoryMock_NoWait.swift
//  Verses
//
//  Created by Darko VUKOTIC on 2.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class MusicRepositoryMock_NoWait: MusicRepositoryObject {
    override func getMusicFileNames(_ callback: @escaping MusicFileNamesCallback) throws {
        let files = [
            "save_me",
            "azan"
        ]

        callback(.Success, files)
    }
}
