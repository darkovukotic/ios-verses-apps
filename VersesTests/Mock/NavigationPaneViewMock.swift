//
//  NavigationPaneViewMock.swift
//  Verses
//
//  Created by Darko VUKOTIC on 15.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class NavigationPaneViewMock: NavigationPaneView {
    func loadPane(_ pane: PaneType) {
        //
    }

    func onAppActive() {
        //
    }

    func onAppInactive() {
        //
    }

    func setBackgroundImage(_ imageFileName: String) {
        //
    }

    func load() {
        //
    }

    func unload() {
        //
    }

    func showMessage(_ message: Message) {
        //
    }

    func setPresenter(_ presenter: PanePresenter) {
        //
    }


}
