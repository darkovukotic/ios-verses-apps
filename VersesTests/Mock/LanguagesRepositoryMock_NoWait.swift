//
//  LanguagesRepositoryMock_NoWait.swift
//  Verses
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class LanguagesRepositoryMock_NoWait: NSObject, LanguagesRepository {
    func getLanguages(_ callback: @escaping LanguagesCallback) throws {
        let languages = [
            try LanguageModel(code:"en", name:"English", fontCode:.LatinFont),
            try LanguageModel(code:"ar", name:"Arabian", fontCode:.ArabicFont),
            try LanguageModel(code:"hi", name:"Hindu", fontCode:.HindiFont),
            try LanguageModel(code:"ru", name:"Russian", fontCode:.CyrillicFont),
            try LanguageModel(code:"tr", name:"Türkçe", fontCode:.LatinFont)
        ]

        callback(CallbackResult.Success, languages)
    }
}
