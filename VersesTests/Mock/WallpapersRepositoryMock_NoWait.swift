//
//  WallpapersRepositoryMock_NoWait.swift
//  Verses
//
//  Created by Darko VUKOTIC on 27.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import UIKit

class WallpapersRepositoryMock_NoWait: NSObject, WallpapersRepository {

    func getWallpapers(_ callback: @escaping WallpapersCallback) throws {
        do {
            let wallpapers = try [WallpaperModel("image1"), WallpaperModel("image2"), WallpaperModel("image3")]
            callback(.Success, wallpapers)

        } catch {
            callback(.Fail, [WallpaperModel]())
        }
    }

}
