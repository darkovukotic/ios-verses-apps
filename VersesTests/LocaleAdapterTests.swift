//
//  LocaleAdapterTests.swift
//  VersesTests
//
//  Created by Darko VUKOTIC on 1.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class LocaleAdapterTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetPhoneLanguageCode() throws {
        let adapter = try Provider.instance.provideLocaleAdapter()

        let code = try adapter.getPhoneLanguageCode()
        XCTAssertEqual("en", code)
    }

    
}
