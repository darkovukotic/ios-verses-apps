//
//  FontsLogicTests.swift
//  VersesTests
//
//  Created by Darko VUKOTIC on 28.2.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class FontsLogicTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testShouldGetFonts() throws {
        let fonts = try FontsLogic.Builder()
            .repository(FontsRepositoryMock_NoWait())
            .build()

        XCTAssertEqual(4, fonts.count())
    }
    
    func testGetFontByCode() throws {
        let fonts = try FontsLogic.Builder()
            .repository(FontsRepositoryMock_NoWait())
            .build()

        var font = try fonts.getFontWithCode(.HindiFont)
        XCTAssertEqual(FontCode.HindiFont, font.getCode())
        XCTAssertEqual("HIMALAYA-TT-FONT", font.getName())
        XCTAssertEqual(22, font.getPreferredSize())

        font = try fonts.getFontWithCode(.ArabicFont)
        XCTAssertEqual(FontCode.ArabicFont, font.getCode())
        XCTAssertEqual("_PDMS_Saleem_QuranFont", font.getName())
        XCTAssertEqual(25, font.getPreferredSize())

        font = try fonts.getFontWithCode(.LatinFont)
        XCTAssertEqual(FontCode.LatinFont, font.getCode())
        XCTAssertEqual("EagleLake-Regular", font.getName())
        XCTAssertEqual(20, font.getPreferredSize())

        font = try fonts.getFontWithCode(.CyrillicFont)
        XCTAssertEqual(FontCode.CyrillicFont, font.getCode())
        XCTAssertEqual("Miroslav-Normal", font.getName())
        XCTAssertEqual(24, font.getPreferredSize())
    }
    
}
