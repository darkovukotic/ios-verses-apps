//
//  PreferencesAdapterTests.swift
//  VersesTests
//
//  Created by Darko VUKOTIC on 1.3.2018.
//  Copyright © 2018 The Young Turks Technology. All rights reserved.
//

import XCTest

class PreferencesAdapterTests: XCTestCase {
    var adapter = PreferencesAdapterObject()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSaveLanguageCodeShouldFailOnEmptyCode() throws {
        do {
            try adapter.saveCurrentLanguageCode("")
            XCTFail("It should have failed")
        }
        catch Exception.InvalidArgument {
            // pass
        }
        catch {
            XCTFail("Unexpected error")
        }
    }
    
    func testSavedLanguageCodeShouldBeRead() throws {
        try adapter.saveCurrentLanguageCode("sr")
        XCTAssertEqual("sr", adapter.getCurrentLanguageCode())

        try adapter.saveCurrentLanguageCode("tr")
        XCTAssertEqual("tr", adapter.getCurrentLanguageCode())

        try adapter.saveCurrentLanguageCode("en")
        XCTAssertEqual("en", adapter.getCurrentLanguageCode())
    }

    func testSavedIsMusicEnabledShouldBeRead() throws {
        try adapter.saveIsMusicEnabled(true)
        XCTAssertEqual(true, adapter.getIsMusicEnabled())

        try adapter.saveIsMusicEnabled(true)
        XCTAssertEqual(true, adapter.getIsMusicEnabled())

        try adapter.saveIsMusicEnabled(false)
        XCTAssertEqual(false, adapter.getIsMusicEnabled())

        try adapter.saveIsMusicEnabled(true)
        XCTAssertEqual(true, adapter.getIsMusicEnabled())
    }

    func testSaveLastSelectedWallpaperIndexShouldFailOnNegativeInt() throws {
        do {
            try adapter.saveLastSelectedWallpaperIndex(-1)
            XCTFail("It should have failed")
        }
        catch Exception.InvalidArgument {
            // pass
        }
        catch {
            XCTFail("Unexpected error")
        }
    }

    func testSavedLastSelectedWallpaperIndexShouldBeRead() throws {
        try adapter.saveLastSelectedWallpaperIndex(1)
        XCTAssertEqual(1, adapter.getLastSelectedWallpaperIndex())

        try adapter.saveLastSelectedWallpaperIndex(2)
        XCTAssertEqual(2, adapter.getLastSelectedWallpaperIndex())

        try adapter.saveLastSelectedWallpaperIndex(0)
        XCTAssertEqual(0, adapter.getLastSelectedWallpaperIndex())
    }
    
}
